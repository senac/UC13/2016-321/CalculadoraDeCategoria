package br.com.senac.calculadoradecategoria;

public class CalculadoraDeCategoria {

    public static final String INFANTIL_A = "Infantial A";
    public static final String INFANTIL_B = "Infantial B";
    public static final String JUVENIL_A = "Juvenil A";
    public static final String JUVENIL_B = "Juvenil B";
    public static final String SENIOR = "Senior";

    public String calcular(Pessoa p) {
        if (p.getIdade() >= 5 && p.getIdade() <= 7) {
            return INFANTIL_A;
        } else if (p.getIdade() <= 10) {
            return INFANTIL_B;
        } else if (p.getIdade() <= 13) {
            return JUVENIL_A;
        } else if (p.getIdade() <= 17) {
            return JUVENIL_B;
        }
        return SENIOR;
    }

}
